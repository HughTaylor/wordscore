# WordScore

Scores five letter words based on the frequency that each letter in the word appears in that position in other words. Useful for optimising wordl strategies by maximising the probability of getting green squares.

### How it works

The program takes a word list as an argument, reduces it to five letter words, then sums up the number of times each letter appears in each position. E.g. the letter "a" appears 341 times as the first letter in a word, 1162 times as the second letter, and so on.

The list of five letter words is then iterated over to calculate a score for each word based on the positional counts in the previous step.

For example (depending on the input list) the word `smoke` might score 2,766 because the letter `s` appears in the first position 854 times in other words, `m` in the second position 365 times, and so on.
