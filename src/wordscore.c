#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int wordlen = 5;

struct Node
{
    char word[4];
    struct Node *next;
};
    
void freeList(struct Node* head)
{
    struct Node* tmp;

    while (head != NULL)
    {
        tmp = head;
        head = head->next;
        free(tmp);
    }

}

void printList(struct Node* head)
{
    struct Node* tmp;
    char word[6];
    int wordCount = 0;

    while (head != NULL)
    {
        tmp = head;
        head = head->next;
        for (int i = 0; i < 5; i++)
        {
            word[i] = tmp->word[i];
        }
        word[5] = '\0';
        printf("%s\n", word);
        wordCount++;
    }
    printf("%i Words\n", wordCount);
}

void calculateLetterScores(struct Node* head, int* letterScores)
{
    struct Node* tmp;
    int *ptr;
    
    while (head != NULL)
    {
        tmp = head;
        head = head->next;
        for (int i = 0; i < 5; i++)
        {
            ptr = letterScores; // Move the pointer back to the start of the array.
            ptr += (5 * (tmp->word[i] - 'a')); // Move the pointer to the correct "row" in the array.
            ptr += i;   // Move the pointer to the correct "column" in the array.
            ++*ptr; // Increase the value in that position by one.
        }
    }
}

void printLetterScores(int *tmp)
{
    for (int i = 0; i < 26; i++)
    {
        printf("%c ", i + 'a');
        for (int j = 0; j < 5; j++)
        {
            printf("%i ", *tmp);
            tmp++;
        }
        printf("\n");
    }
};

void advanceToNextWord(FILE *ptr)
{
    char c;
    do
    {
        c = fgetc(ptr);
    } while (c != '\n');
}

void calculateBestWord(struct Node* head, int* letterScores)
{
    struct Node* tmp;
    char word[6];
    char tmpWord[6];
    int wordScore = 0;
    int tmpScore;

    while (head != NULL)
    {
        tmpScore = 0;
        tmp = head;
        head = head->next;
        for (int i = 0; i < 5; i++)
        {
            tmpWord[i] = tmp->word[i];
            tmpScore += letterScores[(tmp->word[i] - 'a')*5 + i];
        }
        if (tmpScore > wordScore)
        {
            tmpWord[5] = '\0';
            strcpy(word, tmpWord);
            wordScore = tmpScore;
        }
    }
    printf("%s, score: %i\n", word, wordScore);
}

int main(int argc, char *argv[]) {
    
    FILE *inptr;                    // declaration of file pointer
    inptr = fopen(argv[1], "r");    // opening of file
    
    if (!inptr)                     // checking of error
    {
        perror("Error opening file");
        return 1;
    }
    
    int characterNum = 0;
    char c;
    struct Node *root;              // initialise a pointer to a Node
    root = (struct Node *) malloc( sizeof(struct Node) ); // request memory for a Node and point root at it
    root->next = 0;                 // initialise root's next pointer to 0.
    
    struct Node *tmp = root;        // Create a tmp pointer we can move around, initialise to root.
    struct Node *secondLast = root;        // Points to penultimate node. Used to delete last node.
    
    do
    {
        c = fgetc(inptr);           // get a character, advances position indicator by one
        
        if (c == '\n')          // when we meet a newline character
        {
            if (characterNum == wordlen)            // if we are at the 6th letter position
            {
                // create new node
                struct Node *p_new = (struct Node *) malloc( sizeof(struct Node) );
                tmp->next = p_new;
                secondLast = tmp;
                tmp = p_new;
            }
            characterNum = 0;   // always reset characterNum after a newline character
        }
        else if (characterNum > wordlen) // the character is not '\n' AND we are over the word length limit
        {
            while (c != '\n')   // keep going until we hit another new line.
            {
                c = fgetc(inptr); 
            } 
            characterNum = 0;
        } 
        else if ( feof(inptr) ) // if we get to the end of file
        {
            free(tmp);  // delete the node we were writing to
            secondLast->next = 0;   // and set the previous node's "next" to 0;
            break;
        }
        else
        {
            tmp->word[characterNum] = c;    // write the current letter to the current node at the current position.
            characterNum++; // move to the next position.
        }
    }
    while(1);
    fclose(inptr);
    
    // Allocate memory for the letter scores
    int *letterScores;
    letterScores = (int *) calloc(130, sizeof(int));
    
    // Populate it
    calculateLetterScores(root, letterScores);

    printLetterScores(letterScores);
    
    //printList(root);
    calculateBestWord(root, letterScores);
    
    // Don't forget to clean up.
    freeList(root);
    free(letterScores);
    
        
    return  0;
    
}
